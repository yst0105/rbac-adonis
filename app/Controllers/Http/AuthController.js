'use strict'

const User = use('App/Models/User');
class AuthController {

    async login({
        request,
        auth,
        response
    }) {
        const email = request.input("email")
        const password = request.input("password");
        try {
            if (await auth.attempt(email, password)) {
                let user = await User.findBy('email', email)
                let accessToken = await auth.generate(user)
                return response.json({
                    code:200,
                    data: {
                        "user_id": user.id,
                        "user_name": user.username,
                        "access_token": accessToken.token
                    }
                })
            }

        } catch (e) {
            return response.json({
                message: 'You first need to register!'
            })
        }
    }
}

module.exports = AuthController