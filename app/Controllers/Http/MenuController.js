'use strict'

const Menus = use('App/Models/Menu');

class MenuController {
    async test({
        response
    }) {

        try {
            const menus = await Menus.all()
            const menuList = this.getRecursiveMenus(menus.toJSON());

            return response.json({
                message: 'success get Menu List',
                list: menuList
            })
        } catch (e) {
            return response.json({
                message: 'error'
            })
        }
    }

    getRecursiveMenus(arr, parent = 0) {
        let out = []
        for (let i in arr) {
            if (arr[i].idParent == parent) {
                arr[i].isEnabled = Boolean(arr[i].isEnabled)
                let children = this.getRecursiveMenus(arr, arr[i].id)
                if (children) {
                    arr[i].childrens = children
                }
                out.push(arr[i])
            }
        }
        return out
    }

}
module.exports = MenuController