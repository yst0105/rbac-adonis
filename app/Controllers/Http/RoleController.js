'use strict'


class RoleController {
	
	static get inject() {
		return ['App/Services/RoleServices']
	}

	constructor(RoleServices) {
		this._roleServices = RoleServices;
	}
	async getRoleList({
		request,
		response
	}) {
		const input = request.only(['isEnabled', 'page', 'pageSize', 'sort', 'sortName'])
		const RolesList = await this._roleServices.getRoleList(input)

		return response.json({
			code:200,
			message: "成功取得角色列表",
			totalSize: RolesList.total,
			list: RolesList.data
		})
	}

	async getRoles({
		params,
		response
	}) {
		return response.json({
			code:200,
			message: "成功取得單筆角色",
			list: await this._roleServices.getRoles(params.id)
		})
	}
	async createRoles({
		request,
		response
	}) {
		return response.json({
			code:200,
			message: "成功建立角色",
			data:[]
		})
	}
}

module.exports = RoleController