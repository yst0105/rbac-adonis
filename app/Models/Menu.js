'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Menu extends Model {

	children() {
		return this.hasMany('App/Models/Menu', 'id', 'idParent')
	}
}

module.exports = Menu