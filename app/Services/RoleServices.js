'use strict'

const Roles = use('App/Models/Role');

class RoleServices {
	//取得角色清單
	async getRoleList(input) {

		let query = Roles.query();

		input.sort = input.sort || 'desc';
		input.page = input.page || 1;
		input.sortName = input.sortName || 'id';
		input.pageSize = input.pageSize || 10;

		if (input.isEnabled) {
			query = input.isEnabled == 'true' ? query.where('isEnabled', 1) : query.where('isEnabled', 0);
		}

		query = input.sort && input.sortName ? query.orderBy(input.sortName, input.sort) : query.orderBy('id', 'desc')
		query = input.page && input.pageSize ? query.paginate(input.page, input.pageSize) : query.paginate(1, 10);

		const list = await query;
		const RoleList = list.toJSON();
		RoleList.data.forEach((item, key) => {
			item.isEnabled =  item.isEnabled = 1 ? '啟用中':'停用中';

		});
		return RoleList
	}
	async getRoles(id) {

		const roles = await Roles.find(id);
		const roleData = roles.toJSON();
		roleData.isEnabled = Boolean(roleData.isEnabled);
		return roleData
	}
}
module.exports = RoleServices