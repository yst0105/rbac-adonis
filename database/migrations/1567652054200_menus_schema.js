'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MenusSchema extends Schema {
	up() {
		this.create('menus', (table) => {
			table.increments()
			table.string('name', 20).notNullable()
			table.string('code', 20).notNullable()
			table.integer('idParent', 11).defaultTo(0)
			table.string('icon', 20)
			table.integer('isEnabled', 1).defaultTo(1)
			table.integer('createdBy', 11).defaultTo(0)
			table.integer('updatedBy', 11).defaultTo(0)
			table.timestamps()
		})
	}

	down() {
		this.drop('menus')
	}
}

module.exports = MenusSchema