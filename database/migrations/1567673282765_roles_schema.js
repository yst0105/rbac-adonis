'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RolesSchema extends Schema {
	up() {
		this.create('roles', (table) => {
			table.increments()
			table.string('name', 20).notNullable().unique()
			table.integer('isEnabled', 1).defaultTo(1)
			table.integer('createdBy', 11).defaultTo(0)
			table.integer('updatedBy', 11).defaultTo(0)
			table.timestamps()
		})
	}

	down() {
		this.drop('roles')
	}
}

module.exports = RolesSchema